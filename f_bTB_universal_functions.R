# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  #
# 
# Script:
# - f_bTB_universal_functions.R
# 
# Purpose:
# - A set of general useful functions that are used throughout the WP analysis code. 
# - They are not specific to bTB problems, but I are generally useful and used in s
# - To use these, add "source("f_bTB_universal_functions.R")".

# Author: 
# - Anthony Wood, Roslin Institute (Kao group), University of Edinburgh
# 
# Email:
# - anthony.wood@ed.ac.uk
# 
# Date created:
# - 2023-06-07

# Required packages, and why I'm using them
library(crayon)                 # Printing to console in different colours
library(sf)                     # Shapefiles (for defining the Woodchester location as a centroid)
library(leaflet)                # colornumeric, colorfactor

# If P (what I use as a list of params) doesn't exist (all my parameters), make it.
if(!exists("P")){P = list()}



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  #
# (1) Function to print to console with colour
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

# Function that prints to the console in different colours.
print2 = function(x, ovr = FALSE, # Do we want to overwrite the previous print?
                  delay = 0){
  text = ifelse(!ovr, paste0(x, "\n"), paste0("\r", x))
  cat(bold(inverse(blue(paste0(text)))))
  Sys.sleep(delay)}



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  #
# (2) Conditional mutate
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

# Neat little conditional mutate (instead of ifelse'ing everything)
# From G. Grothendieck, stackoverflow 34096162
mutate_cond =
  function(.data, condition, ..., envir = parent.frame()) {
    condition <- eval(substitute(condition), .data, envir)
    .data[condition,] <- .data[condition,] %>% mutate(...)
    .data
  }



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  #
# (3) Full join
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

# Full outer join function of two columns x and y. If x has N entries and y has Y entries, the output is a NM x 2 matrix
full_outer_join = function(x,y, nameX = "x", nameY = "y"){
  tibble(x) %>% 
    full_join(tibble(y), by=character()) %>% 
    setNames(c(nameX%>%as.character(), nameY %>% as.character()))}


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  #
# (4) Shorthand for factorial and kronecker delta
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

# Custom functions
fct = function(x){factorial(x)}       # Factorial function (just a name change)
kde = function(x){ifelse(x == 0, 1, 0)} # Kronecker delta function (just a name change)



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  #
# (5) Turn a date into a fractional date (1st February 1992 -> 1992.08)
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

fractional_date = function(date) as.numeric(as.Date(date) - as.Date(format(as.Date(date), "%Y-01-01"))+1)/365.25 + format(as.Date(date), "%Y") %>% as.numeric

reverse_fractional_date = function(fractional){
  year = floor(fractional)
  num.days = floor((fractional - year) * 365.25)
  ymd(paste0(floor(fractional),"/01/01")) + days(num.days)
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  #
# (6) Colour palettes for likelihoods, transitions, pairs etc.
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

P$Col$Lines = colorNumeric("magma", domain = 0:1)

P$Col$Transitions = tibble(Transition = c("B-B", "C-C", "C-B", "B-C", "C-O", "O-C", "B-O", "O-B", "O-O", "C-X-C"),
                           Transition.Colour = c("blue", "brown1", "darkorange3", "darkgreen", "grey", "grey", "grey", "grey", "grey","lightpink2"))

P$Col$Hosts = tibble(Host = c("badger", "cow", "bovine", "other", "other_wildlife"),
                     Host.Colour = c("blue", rep("brown1",2), rep("orange1",2)),
                     Host.Label = c("Badger", rep("Cattle",2), rep("Other",2)))

P$Col$Species = colorFactor(palette = P$Col$Hosts$Host.Colour, levels=P$Col$Hosts$Host)



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  #
# (7) Function to fit a reasonable gamma distribution given we just know a mean and standard deviation
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

f_Find.Sensible.Gamma.Distribution = function(pmean.target, p050.target = NA, p950.target = NA, p025.target = NA, p975.target = NA){ # Input: mean, confidence interval

  # pmean.target = 11.1
  # p05.target = 3.29
  # p95.target = 25.7
  
  # Sample a whole bunch of gamma distribution params (shape, rate) that have the mean we want
  ga.shape.list = seq(0.05,10,length.out = 1000)
  ga.rate.list = ga.shape.list / pmean.target
    
    gammatests =
      tibble(ga.shape = ga.shape.list, ga.rate = ga.rate.list) %>% # Under a whole selection of gamma-distributed RVs
      rowwise %>% 
      mutate(fundist = rgamma(100000, shape = ga.shape, rate = ga.rate) %>% list,   # Generate a bunch of gamma distributed random vars under these params...
             p050 = fundist %>% quantile(0.050), # Evaluate the  5th %ile
             p950 = fundist %>% quantile(0.950), # Evaluate the 95th %ile
             p025 = fundist %>% quantile(0.025), # Evaluate the 2.5th %ile
             p975 = fundist %>% quantile(0.975), # Evaluate the 97.5th %ile
             pmean2 = fundist %>% mean, # Calculate the analytic mean 
             pmean = ga.shape / ga.rate,# Calculate the analytic mean 
             p050.diff = abs(p050 - p050.target),
             p950.diff = abs(p950 - p950.target),
             p025.diff = abs(p025 - p025.target),
             p975.diff = abs(p975 - p975.target),
             pmean.diff = abs(pmean - pmean.target)) %>%
      select(-fundist) %>%
      ungroup %>%
      mutate(totaldiff.90 = p050.diff + 0.05*(p950.diff) + pmean.diff, # Not so concerned about the p95 being precise - 5th and mean more important
             totaldiff.95 = p025.diff + 0.05*(p975.diff) + pmean.diff # Not so concerned about the p95 being precise - 5th and mean more important
      ) 
    
    if(is.na(p975.target)){
      gammatests = gammatests %>% arrange(totaldiff.90)}
    else{
      gammatests = gammatests %>% arrange(totaldiff.95)}
    
    return(list(shape = gammatests$ga.shape[1],
                rate = gammatests$ga.rate[1]))
    
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  #
# (8) Where is Woodchester Park located?
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

P$WP.Location = 
  tibble(xcoord = 382244, ycoord = 201149) %>%
  st_as_sf(coords=c("xcoord","ycoord"), crs = 27700) %>% # EPSG code for grid references
  st_transform(4326)
